from CommonTools import sh
import json


class SysFetch:
    def __init__(self) -> None:
        self.os = ""
        self.pkgm = ""
        self.root_cmd = ""

    def get_os_name(self) -> str:
        return sh("grep 'ID' /etc/os-release").stdout.splitlines()[0].split('"')[1]

    def match_pkgm(self, os: str) -> str:
        pkgm = ""
        match os:
            case "debian" | "ubuntu" | "mint":
                pkgm = "apt"
            case "fedora":
                pkgm = "dnf"
            case "void":
                pkgm = "xbps"
            case "arch":
                pkgm = "pacman"
            case str():
                raise KeyError("System not supported")
        return pkgm

    def reconfigure(self) -> None:
        self.os = self.get_os_name()
        self.pkgm = self.match_pkgm(self.os)
        with open("./data.jsonc", mode="w") as data:
            json.dump(
                {"OS": self.os, "PKGM": self.pkgm, "ROOT_CMD": self.root_cmd},
                data,
                indent=2,
            )
