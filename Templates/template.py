from sys import path as sys_path
from sys import exit
from os import path as os_path
from os import getcwd as os_getcwd


try:
    sys_path.insert(0, os_path.join(os_getcwd(), ".."))
    sys_path.insert(0, os_getcwd())

    from CommonTools import sh
    from numpy._typing import NDArray
    from numpy import array

except ModuleNotFoundError as error:
    exit(f"{error.msg}\n{error.path}")


class BasePkgm:
    """
    Template for the package manager class.
    """

    def __init__(self) -> None:
        self.name: str = ""

    def get_pkgs(self, command: str) -> list[str]:
        """
        This function processes the output of the
        package manager search command by dividing
        it into a tuple.
        """
        return sh(command).stdout.splitlines()

    def get_installed_pkgs(self) -> NDArray:
        """
        This function returns the dictionary of installed packages.
        """
        return array(array(("pkg", "version")))

    def get_repo_pkgs(self) -> NDArray:
        """
        This function returns the dictionary of packages in repositories.
        """
        return array(array(("pkg", "version")))

    def get_update_list(self) -> None:
        """
        This function returns the dictionary of packages to update.
        """
        return

    def root_access(self) -> str:
        ...
        return "0=p]-["

    def change_pkgs(self, command: str, pkgs: dict | list | tuple | str) -> bool:
        """
        This function installs or updates or removes packages
        and returns True if action was succesfull
        """
        change_cmd = f"echo {self.root_access()} | sudo -S {command}"
        match pkgs:
            case str():
                change_cmd += pkgs
            case list() | tuple():
                change_cmd += " ".join(pkgs)
            case dict():
                change_cmd += " ".join(pkgs.keys())
            case _:
                return False
        print(change_cmd)
        sh(change_cmd)
        return True

    def install_pkgs(self, pkgs) -> bool:
        if pkgs:
            return True
        return False

    def update_pkgs(self, pkgs) -> bool:
        if pkgs:
            return True
        return False
