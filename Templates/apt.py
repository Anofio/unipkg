import sys
import os

try:
    sys.path.insert(0, os.path.join(os.getcwd(), ".."))
    sys.path.insert(0, os.getcwd())

    from Templates.template import BasePkgm

    class AptPkgm(BasePkgm):
        def __init__(self) -> None:
            super().__init__()

        def install_pkgs(self, pkgs: list | tuple | str) -> bool:
            return self.change_pkgs("apt install", pkgs)

        def update_pkgs(self, pkgs: list | tuple | str) -> bool:
            return self.change_pkgs("apt update", pkgs) and self.change_pkgs(
                "apt upgrade ", pkgs
            )

except ModuleNotFoundError as error:
    print(error)
