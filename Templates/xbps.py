from sys import path as sys_path
from sys import exit
from os import path as os_path
from os import getcwd as os_getcwd


try:
    sys_path.insert(0, os_path.join(os_getcwd(), ".."))
    sys_path.insert(0, os_getcwd())

    from numpy._typing import NDArray
    from numpy import matrix, array
    from Templates.template import BasePkgm

except ModuleNotFoundError as error:
    exit(f"{error.name}\n{error.path}")


class XbpsPkgm(BasePkgm):
    """
    template for void linux xbps package manager.
    """

    def __init__(self) -> None:
        super().__init__()
        self.name = "xbps"

    def xbps_separate_package_and_version(self, pkg_version: str) -> NDArray:
        """
        xdg-desktop-portal-0.7.1_1 -> ['xdg-desktop-portal', '0.7.1_1'].
        """
        return array(
            [
                pkg_version[: pkg_version.rfind("-")],
                pkg_version[pkg_version.rfind("-") + 1 :],
            ]
        )

    def get_xbps_pkgs(self, command: str) -> NDArray:
        """
        Converts lines from xbps package manager to dict.
        """
        pkgs = self.get_pkgs(command)
        installed_pkgs_info = matrix(
            array(
                [
                    self.xbps_separate_package_and_version(pkg_info.split()[1])
                    for pkg_info in pkgs
                ]
            )
        )
        return installed_pkgs_info

    def install_pkgs(self, pkgs: list[str] | tuple | str) -> bool:
        super().install_pkgs(pkgs)
        return self.change_pkgs("xbps-install -Sy ", pkgs)

    def update_pkgs(self, pkgs: list[str] | tuple | str) -> bool:
        super().update_pkgs(pkgs)
        return self.change_pkgs("xbps-install -Syu ", pkgs)

    def get_installed_pkgs(self) -> NDArray:
        super().get_installed_pkgs()
        return self.get_xbps_pkgs("xbps-query -s .")

    def get_repo_pkgs(self) -> NDArray:
        super().get_repo_pkgs()
        return self.get_xbps_pkgs("xbps-query -Rs .")
