#!/usr/bin/env python
from Templates import BasePkgm, XbpsPkgm, AptPkgm


class UniPackageManager:
    """
    A package manager that doesn't care about your distro.
    """

    def __init__(self, pkg_manager: str):
        """
        To add your package manager, use template.py and
        ready-made package manager templates to create your
        own. Put it in the Templates directory, import it and
        specify in case statement.
        """
        self.pkgm: BasePkgm = BasePkgm()

        match pkg_manager:
            case "xbps":
                self.pkgm = XbpsPkgm()
            case "apt":
                self.pkgm = AptPkgm()
            case str() as unsupported:
                raise KeyError(f"Unsupported pkg manager: {unsupported}.")
            case wrong_type:
                raise TypeError(
                    f"Package manager name must be str and not "
                    f"{type(wrong_type): wrong_type}."
                )
        self.get_installed_pkgs = self.pkgm.get_installed_pkgs
        self.get_repo_pkgs = self.pkgm.get_repo_pkgs
        self.install_pkgs = self.pkgm.install_pkgs
        self.update_pkgs = self.pkgm.update_pkgs
