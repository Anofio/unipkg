from Project.CommonTools import sh
import json


class SysFetch:
    """
    Получить информацию о системе
    Get system info
    """

    def __init__(self) -> None:
        self.os = ""
        self.pkgm = ""
        self.root_cmd = ""

    def get_os_name(self) -> str:
        os_name = (
            sh("grep 'ID' /etc/os-release")
            .stdout.splitlines()[0]
            .split("=")[1]
            .replace('"', "")
        )
        return os_name

    def match_pkgm(self, os: str) -> str:
        pkgm = ""
        match os:
            case "void":
                pkgm = "xbps"
            case "arch":
                pkgm = "pacman"
            case str():
                raise KeyError("System is not supported")
        return pkgm

    def reconfigure(self) -> None:
        self.os = self.get_os_name()
        self.pkgm = self.match_pkgm(self.os)
        with open("./Project/Data/system.jsonc", mode="w") as data:
            json.dump(
                {"OS": self.os, "PKGM": self.pkgm},
                data,
                indent=2,
            )
