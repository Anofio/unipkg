#!/usr/bin/env python
from Project.Templates import BasePkgm, XbpsPkgm, PacmanPkgm


class UniPackageManager:
    """
    A package manager that doesn't care about your distro.
    """

    def __init__(self, pkg_manager: str):
        """
        Чтобы добавить свой менеджер пакетов, используйте template.py и
        готовые шаблоны менеджеров пакетов для создания вашего
        собственного. Поместите его в каталог Templates, импортируйте и
        укажите в math statement.
        To add your package manager, use template.py and
        ready-made package manager templates to create your
        own. Put it in the Templates directory, import it and
        specify in case statement.
        """
        self.pkgm: BasePkgm = BasePkgm()

        match pkg_manager:
            case "xbps":
                self.pkgm = XbpsPkgm()
            case "pacman":
                self.pkgm = PacmanPkgm()
            case str() as unsupported:
                raise KeyError(f"Unsupported pkg manager: {unsupported}.")
            case wrong_type:
                raise TypeError(
                    f"Package manager name must be str and not "
                    f"{type(wrong_type): wrong_type}."
                )
        self.install_pkgs = self.pkgm.install_pkgs
        self.update_pkgs = self.pkgm.update_pkgs
        self.remove_pkgs = self.pkgm.remove_pkgs
        self.clean_orphans = self.pkgm.clean_orphans
        self.get_pkgs_info = self.pkgm.get_pkgs_info
        self.dump_pkgs_data = self.pkgm.dump_pkgs_data
