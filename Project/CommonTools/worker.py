from PyQt6.QtCore import QThread, pyqtSignal


class Worker(QThread):
    """
    Запускаемый QThread
    Runable QThread
    """

    task_completed = pyqtSignal(str)

    def __init__(self, task) -> None:
        super().__init__()
        self.task = task

    def run(self):
        self.task_completed.emit(self.task())
