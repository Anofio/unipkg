#!/usr/bin/env python

import subprocess


def sh(command: str) -> subprocess.CompletedProcess[str]:
    """
    Получить вывод команды терминала
    Get terminal command output
    """
    return subprocess.run(
        command, shell=True, universal_newlines=True, stdout=subprocess.PIPE
    )


if __name__ == "__main__":
    sh(input())
