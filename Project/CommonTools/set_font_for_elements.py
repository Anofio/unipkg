from PyQt6.QtGui import QFont, QFontDatabase


def set_font_for_elements(
    font_path: str, elements: list | tuple, font_size: int = 12, font_style: int = 0
) -> None:
    """
    Установить шрифт для элементов PyQT
    Set font for PyQT elements
    """
    font = QFont(
        QFontDatabase.applicationFontFamilies(
            QFontDatabase.addApplicationFont(font_path)
        )[font_style],
        font_size,
    )

    for element in elements:
        element.setFont(font)
