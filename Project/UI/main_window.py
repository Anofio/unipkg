from Project.CommonTools import Worker, sh, set_font_for_elements
from Project.UI.about import About
from Project.UI.Dialogs import ChangePackagesDialog, UpdateDialog
from Project.UI.Dialogs.password_dialog import PasswordDialog
from Project.unipkg import UniPackageManager
from PyQt6.QtWidgets import (
    QAbstractItemView,
    QHeaderView,
    QMainWindow,
    QPushButton,
    QTableWidget,
    QLabel,
    QLineEdit,
    QTableWidgetItem,
)
from PyQt6.QtCore import Qt
from PyQt6.uic import load_ui
from sys import path as sys_path
from os import getcwd as os_getcwd, path as os_path
import json

sys_path.insert(0, os_path.join(os_getcwd(), ".."))
sys_path.insert(0, os_getcwd())


class MainWindowWorker(Worker):
    def __init__(self, task) -> None:
        super().__init__(task)

    def await_internet_connection(self) -> str:
        """
        Ожидание соединения с интернетом
        Waiting for Internet connection
        """
        while True:
            if len(sh("ping ya.ru -c 1").stdout.split("\n")) != 1:
                return "Completed"
            else:
                __import__("time").sleep(5)

    def update_package_data(self) -> str:
        """
        Обновление информации о пакетах
        Update package information
        """
        with open("./Project/Data/system.jsonc", "r") as data, open(
            "./Project/Data/pkgs.jsonc", "w+"
        ) as pkgs_data:
            data_json = json.load(data)
            self.pkgm_name = data_json["PKGM"]
            self.unipkg = UniPackageManager(self.pkgm_name)
            self.unipkg.dump_pkgs_data()
            self.pkgs = json.load(pkgs_data)

        self.installed_pkgs = [
            pkg for pkg in self.pkgs.keys() if self.pkgs[pkg]["Installed"] != "-"
        ]
        self.to_update_list = [
            pkg
            for pkg in self.installed_pkgs
            if self.pkgs[pkg]["Installed"] < self.pkgs[pkg]["Repo"]
        ]
        return "Completed"

    def retrieve_checkbox_values(self, pkgs_table: QTableWidget) -> str:
        """
        Получить список пакетов для установки и удаления
        Get a list of packages to install and remove
        """
        package_info = [
            (
                pkgs_table.item(row, 0).text(),
                pkgs_table.item(row, 4).checkState(),
                pkgs_table.item(row, 2).text(),
            )
            for row in range(pkgs_table.rowCount())
        ]

        self.to_install_list = [
            package[0]
            for package in package_info
            if package[1] == Qt.CheckState.Checked and package[2] == "-"
        ]
        self.to_remove_list = [
            package[0]
            for package in package_info
            if package[1] != Qt.CheckState.Checked and package[2] != "-"
        ]

        return "Completed"

    def change_pkgs(
        self,
        unipkg: UniPackageManager,
        password: str,
        to_install_list: list = [],
        to_remove_list: list = [],
        to_update_list: list = [],
        clean_orphans: bool = False,
    ) -> str:
        if to_install_list:
            unipkg.install_pkgs(to_install_list, password)
        if to_remove_list:
            unipkg.remove_pkgs(to_remove_list, password)
        if to_update_list and not to_remove_list and not clean_orphans:
            unipkg.update_pkgs(
                [pkg for pkg in to_update_list if pkg not in to_remove_list], password
            )
        if clean_orphans:
            unipkg.clean_orphans(password)
        return "Completed"


class MainWindow(QMainWindow):
    """
    Главное окно и работа с дочерними окнами
    Connect main window child windows
    """

    def __init__(self) -> None:
        super().__init__()

        self.about_button = QPushButton()
        self.update_button = QPushButton()
        self.clean_button = QPushButton()
        self.confirm_button = QPushButton()
        self.search_button = QPushButton()
        self.pkgs_table = QTableWidget()
        self.label_status = QLabel()
        self.label_pkgs_amount = QLabel()
        self.label_to_update = QLabel()
        self.search_line = QLineEdit()

        self.to_install_list = []
        self.to_remove_list = []
        self.to_update_list = []
        self.clean_orphans = False

    def init_ui(self) -> None:
        load_ui.loadUi("./Project/UI/Assets/main_window.ui", self)

        # Установка шрифтов для кнопок
        # Set fonts for buttons
        set_font_for_elements(
            "./Project/UI/Assets/FiraCodeNerdFont-Medium.ttf",
            [
                self.about_button,
                self.update_button,
                self.search_button,
                self.confirm_button,
            ],
        )
        set_font_for_elements(
            "./Project/UI/Assets/FiraCodeNerdFont-Medium.ttf", [self.clean_button], 13
        )

        self.set_table_ui()
        self.setup_await_internet_connection_worker()
        self.connect_functions()

    def set_table_ui(self):
        """
        Установка флагов для элементов таблицы
        Setting flags for table elements
        """
        self.pkgs_table.setEditTriggers(QAbstractItemView.EditTrigger.NoEditTriggers)
        self.pkgs_table.setSelectionMode(
            QAbstractItemView.SelectionMode.SingleSelection
        )
        self.pkgs_table.setSelectionBehavior(
            QAbstractItemView.SelectionBehavior.SelectRows
        )
        self.pkgs_table.setSortingEnabled(False)
        self.pkgs_table.horizontalHeader().setSectionResizeMode(
            1, QHeaderView.ResizeMode.Stretch
        )
        self.pkgs_table.setColumnWidth(0, 300)
        self.pkgs_table.setColumnWidth(2, 200)
        self.pkgs_table.setColumnWidth(3, 200)
        self.pkgs_table.setColumnWidth(4, 10)

    def setup_await_internet_connection_worker(self) -> None:
        """
        Запустить поток функции ожидания соединения с интернетом
        Start the thread of the internet connection waiting function
        """
        self.worker_await_internet_connection = MainWindowWorker(
            self.await_internet_connection
        )
        self.worker_await_internet_connection.start()
        self.worker_await_internet_connection.task_completed.connect(
            self.await_internet_connection_completed
        )

    def await_internet_connection(self) -> None:
        self.worker_await_internet_connection.await_internet_connection()

    def await_internet_connection_completed(self) -> None:
        self.label_status.setText("Status: Fetching repos...")
        self.worker_await_internet_connection.quit()
        self.setup_update_package_data_worker()

    def setup_update_package_data_worker(self) -> None:
        """
        Запустить поток функции обновления информации о пакетах
        Start the thread of the package data updating function
        """
        self.worker_update_package_data = MainWindowWorker(self.update_package_data)
        self.worker_update_package_data.start()
        self.worker_update_package_data.task_completed.connect(
            self.update_package_data_completed
        )

    def update_package_data(self) -> None:
        self.worker_update_package_data.update_package_data()

    def update_package_data_completed(self) -> None:
        """
        Обновление содержимого окна
        Updating window contents
        """
        self.setWindowTitle(f"Unipkg - {self.worker_update_package_data.pkgm_name}")
        self.init_label()
        self.fill_table()
        self.label_status.setText("Status: Ready")
        if self.search_line.text():
            self.filter_table()
        self.pkgs_table.viewport().update()
        self.unipkg = self.worker_update_package_data.unipkg
        self.to_update_list = self.worker_update_package_data.to_update_list
        self.worker_update_package_data.quit()

    def init_label(self) -> None:
        # Оторбражение информации о пакетах
        # Display package information

        self.label_pkgs_amount.setText(
            f"Packages: {str(len(self.worker_update_package_data.installed_pkgs))}"
        )

        self.label_to_update.setText(
            f"To update: {str(len(self.worker_update_package_data.to_update_list))}"
        )

    def fill_table(self) -> None:
        # Создание элементов таблицы пакетов
        # Create package table entries
        self.pkgs_table.setRowCount(0)
        for enum in enumerate(self.worker_update_package_data.pkgs):
            currentRowCount = self.pkgs_table.rowCount()
            self.pkgs_table.insertRow(currentRowCount)

            current_pkg = self.worker_update_package_data.pkgs[enum[1]]
            pkg_name = QTableWidgetItem(enum[1])

            local_version = QTableWidgetItem(current_pkg["Installed"])
            repo_version = QTableWidgetItem(current_pkg["Repo"])
            desk = QTableWidgetItem(current_pkg["Description"])
            checkbox = QTableWidgetItem()

            # Установка флагов для элементов таблицы
            # Setting flags for table elements
            for item in (pkg_name, desk, local_version, repo_version, checkbox):
                item.setFlags(Qt.ItemFlag.NoItemFlags)
                if item != desk:
                    item.setTextAlignment(Qt.AlignmentFlag.AlignCenter)
            checkbox.setFlags(
                Qt.ItemFlag.ItemIsUserCheckable | Qt.ItemFlag.ItemIsEnabled
            )
            if local_version.text() != "-":
                checkbox.setCheckState(Qt.CheckState.Checked)
            else:
                checkbox.setCheckState(Qt.CheckState.Unchecked)

            def set_item(col: int, item: QTableWidgetItem) -> None:
                # Установить элемент таблицы
                # Set table element
                self.pkgs_table.setItem(enum[0], col, item)

            set_item(0, pkg_name)
            set_item(1, desk)
            set_item(2, local_version)
            set_item(3, repo_version)
            set_item(4, checkbox)

    def filter_table(self) -> None:
        # Поиск по таблице
        # Search by element in table
        if self.label_status.text() not in (
            "Status: Ready",
            "Status: Performing actions...",
        ):
            return
        filter_text = self.search_line.text().lower().replace(" ", "")
        for row in range(self.pkgs_table.rowCount()):
            pkg_name = self.pkgs_table.item(row, 0).text().lower().replace(" ", "")
            pkg_desc = self.pkgs_table.item(row, 1).text().lower().replace(" ", "")
            self.pkgs_table.setRowHidden(row, filter_text not in pkg_name + pkg_desc)
        self.pkgs_table.viewport().update()

    def setup_retrieve_checkbox_values(self) -> None:
        if self.label_status.text() == "Status: Ready":
            self.worker_retrive_checkbox_values = MainWindowWorker(
                self.retrieve_checkbox_values
            )
            self.worker_retrive_checkbox_values.start()
            self.worker_retrive_checkbox_values.task_completed.connect(
                self.retrieve_checkbox_values_completed
            )

    def retrieve_checkbox_values(self) -> None:
        self.worker_retrive_checkbox_values.retrieve_checkbox_values(self.pkgs_table)

    def retrieve_checkbox_values_completed(self) -> None:
        # Обработка чекбоксов
        # Checkboxes processing
        if (
            self.worker_retrive_checkbox_values.to_remove_list
            or self.worker_retrive_checkbox_values.to_install_list
        ):
            self.label_status.setText("Status: Performing actions...")
            self.to_install_list = self.worker_retrive_checkbox_values.to_install_list
            self.to_remove_list = self.worker_retrive_checkbox_values.to_remove_list
            self.worker_retrive_checkbox_values.quit()
            self.confirm_show()

    def confirm_show(self) -> None:
        """
        Показать окно подтверждения (для установки или удаления пакетов)
        Show confirm window (for installing or removing packages)
        """
        self.change_packages_dialog = ChangePackagesDialog(
            self.to_install_list,
            self.to_remove_list,
        )

        self.change_packages_dialog.show()
        self.change_packages_dialog.closed.connect(self.confirm_completed)

    def update_show(self) -> None:
        """
        Показать окно подтверждения (для обновления пакетов)
        Show confirm window (for updating packages)
        """
        if self.label_status.text() == "Status: Ready" and self.to_update_list:
            self.label_status.setText("Status: Performing actions...")
            self.update_dialog = UpdateDialog(self.to_update_list)
            self.update_dialog.show()
            self.update_dialog.closed.connect(self.update_completed)

    def clean_show(self) -> None:
        """
        Показать окно подтверждения (для отчистки ненужных зависимостей)
        Show confirm window (for orphans cleaning)
        """
        if self.label_status.text() == "Status: Ready":
            self.label_status.setText("Status: Performing actions...")
            self.clean_dialog = PasswordDialog()
            self.clean_dialog.init_single()
            self.clean_dialog.show()
            self.clean_dialog.closed.connect(self.clean_completed)

    def about_show(self) -> None:
        """
        Показать окно с информацией о приложении
        Show about window
        """
        self.about = About()
        self.about.show()

    def confirm_completed(self) -> None:
        self.process_password(self.change_packages_dialog.root_passwd)

    def update_completed(self) -> None:
        self.process_password(self.update_dialog.root_passwd)

    def clean_completed(self) -> None:
        self.clean_orphans = True
        self.process_password(self.clean_dialog.root_passwd)

    def process_password(self, password: str) -> None:
        """
        Обработка полученного пароля root
        Root password processing
        """
        if password:
            self.password = password
            self.setup_change_pkgs_worker()
        else:
            self.password = password
            self.setup_update_package_data_worker()

    def setup_change_pkgs_worker(self) -> None:
        """
        Запустить поток функции удаления / установки пакетов
        Sthread of the package installing / removing
        """
        self.worker_change_pkgs = MainWindowWorker(self.change_pkgs)
        self.worker_change_pkgs.start()
        self.worker_change_pkgs.task_completed.connect(self.change_pkgs_completed)

    def change_pkgs(self) -> None:
        self.worker_change_pkgs.change_pkgs(
            self.unipkg,
            self.password,
            self.to_install_list,
            self.to_remove_list,
            self.to_update_list,
            self.clean_orphans,
        )
        self.clean_orphans = False

    def change_pkgs_completed(self) -> None:
        self.password = ""
        self.worker_change_pkgs.quit()
        self.setup_update_package_data_worker()

    def connect_functions(self) -> None:
        # Конект функций
        # function connection
        self.confirm_button.clicked.connect(self.setup_retrieve_checkbox_values)
        self.update_button.clicked.connect(self.update_show)
        self.clean_button.clicked.connect(self.clean_show)
        self.about_button.clicked.connect(self.about_show)
        self.search_button.clicked.connect(self.filter_table)
        self.search_line.returnPressed.connect(self.filter_table)
