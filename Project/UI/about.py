from PyQt6.QtWidgets import QDialog, QLabel, QPushButton
from PyQt6.uic import load_ui
from PyQt6.QtCore import Qt
from json import load


class About(QDialog):
    def __init__(self) -> None:
        super().__init__()
        self.label_name = QLabel()
        self.label_author = QLabel()
        self.label_tg = QLabel()
        self.label_git = QLabel()
        self.label_color_scheme = QLabel()
        self.label_version = QLabel()
        self.exit_button = QPushButton()
        self.init_ui()

    def init_ui(self) -> None:
        load_ui.loadUi("./Project/UI/Assets/about.ui", self)

        for label in (self.label_tg, self.label_git, self.label_color_scheme):
            label.setTextFormat(Qt.TextFormat.RichText)
            label.setTextInteractionFlags(Qt.TextInteractionFlag.LinksAccessibleByMouse)
            label.setOpenExternalLinks(True)

        # Configure QLabel instances for rich text and external links
        # Настройка экземпляры QLabel для форматированного текста и внешних ссылок.
        with open("./Project/Data/app.jsonc", "r") as app_data:
            app_data_json = load(app_data)
            self.label_tg.setText(
                f"Telegram: <a href=\"{app_data_json['tg_url']}\", "
                f"style=\"color: #89b4fa;\">{app_data_json['tg_url_short']}</a>"
            )

            self.label_git.setText(
                f"Git: <a href=\"{app_data_json['git_url']}\", "
                f"style=\"color: #89b4fa;\">{app_data_json['git_url_short']}</a>"
            )

            self.label_color_scheme.setText(
                f"Color scheme: <a href=\"{app_data_json['color_url']}\", "
                f"style=\"color: #89b4fa;\">{app_data_json['color_url_short']}</a>"
            )

            self.label_version.setText(f"Version: {app_data_json['version']}")

        self.exit_button.clicked.connect(self.close)
