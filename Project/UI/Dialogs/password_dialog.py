from Project.CommonTools.set_font_for_elements import set_font_for_elements
from Project.CommonTools.sh import sh
from PyQt6.QtWidgets import QLabel, QLineEdit, QPushButton, QDialog
from PyQt6.QtCore import pyqtSignal
from PyQt6.QtTest import QTest
from PyQt6.uic import load_ui


class PasswordDialog(QDialog):
    closed = pyqtSignal()

    def closeEvent(self, event) -> None:
        super(PasswordDialog, self).closeEvent(event)
        self.closed.emit()

    def __init__(self) -> None:
        super().__init__()
        self.lineedit_password = QLineEdit()
        self.password_vis_toggle_button = QPushButton()
        self.confirm_password_button = QPushButton()
        self.label_root_password = QLabel()

        self.root_passwd = ""

    def init_buttons(self):
        set_font_for_elements(
            "./Project/UI/Assets/FiraCodeNerdFont-Medium.ttf",
            [self.password_vis_toggle_button, self.confirm_password_button],
        )
        self.password_vis_toggle_button.setDefault(False)
        self.confirm_password_button.setDefault(True)

        self.lineedit_password.setEchoMode(QLineEdit.EchoMode.Password)

        self.connect_functions()

    def init_single(self) -> None:
        load_ui.loadUi("./Project/UI/Dialogs/Assets/password.ui", self)
        self.init_buttons()

    def init_ui(self) -> None:
        return

    def toggle_passwd_vis(self):
        match self.password_vis_toggle_button.text():
            case "󰈉":
                self.lineedit_password.setEchoMode(QLineEdit.EchoMode.Password)
                self.password_vis_toggle_button.setText("󰈈")
            case "󰈈":
                self.lineedit_password.setEchoMode(QLineEdit.EchoMode.Normal)
                self.password_vis_toggle_button.setText("󰈉")
            case _:
                pass

    def confrim(self):
        command_output = sh(
            f"echo {self.lineedit_password.text()} | sudo -S echo 1"
        ).stdout.splitlines()
        if not command_output:
            QTest.qWait(100)
            self.label_root_password.setText("Password Incorrect!")
            QTest.qWait(5000)
            self.label_root_password.setText("Root password:")
            return
        self.root_passwd = self.lineedit_password.text()
        self.close()

    def connect_functions(self):
        self.password_vis_toggle_button.clicked.connect(self.toggle_passwd_vis)
        self.confirm_password_button.clicked.connect(self.confrim)
