from Project.UI.Dialogs.password_dialog import PasswordDialog
from PyQt6.QtWidgets import QLabel, QListWidget
from PyQt6.uic import load_ui


class ChangePackagesDialog(PasswordDialog):
    def __init__(
        self, packages_to_install: list[str], packages_to_remove: list[str]
    ) -> None:
        super().__init__()
        self.packages_to_install = packages_to_install
        self.packages_to_remove = packages_to_remove

        self.list_to_install = QListWidget()
        self.list_to_remove = QListWidget()
        self.label_to_install = QLabel()
        self.label_to_install_amount = QLabel()
        self.label_to_remove = QLabel()
        self.label_to_remove_amount = QLabel()

        self.init_ui()

    def init_ui(self) -> None:
        super().init_ui()
        load_ui.loadUi("./Project/UI/Dialogs/Assets/change_packages_dialog.ui", self)

        self.update_install_list()
        self.update_remove_list()

        self.init_buttons()

    def update_install_list(self) -> None:
        self.label_to_install_amount.setText(f"({len(self.packages_to_install)})")
        self.list_to_install.addItems(self.packages_to_install)

    def update_remove_list(self) -> None:
        self.label_to_remove_amount.setText(f"({len(self.packages_to_remove)})")
        self.list_to_remove.addItems(self.packages_to_remove)
