from Project.UI.Dialogs.password_dialog import PasswordDialog
from PyQt6.QtWidgets import QLabel, QListWidget
from PyQt6.uic import load_ui


class UpdateDialog(PasswordDialog):
    def __init__(self, packages_to_update) -> None:
        super().__init__()
        self.packages_to_update = packages_to_update

        self.list_to_update = QListWidget()
        self.label_to_update = QLabel()
        self.label_to_update_amount = QLabel()

        self.init_ui()

    def init_ui(self) -> None:
        super().init_ui()
        load_ui.loadUi("./Project/UI/Dialogs/Assets/update_dialog.ui", self)

        self.update_update_list()

        self.init_buttons()

    def update_update_list(self) -> None:
        self.label_to_update_amount.setText(f"({len(self.packages_to_update)})")
        self.list_to_update.addItems(self.packages_to_update)
