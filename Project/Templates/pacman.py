from sys import path as sys_path
from sys import exit
from os import path as os_path
from os import getcwd as os_getcwd


try:
    sys_path.insert(0, os_path.join(os_getcwd(), ".."))
    sys_path.insert(0, os_getcwd())

    from Project.Templates.template import BasePkgm

except ModuleNotFoundError as error:
    exit(f"{error.name}\n{error.path}")


class PacmanPkgm(BasePkgm):
    """
    Обработчик для пакетного менеджера pacman от arch linux
    Handler for arch linux pacman package manager
    """

    def __init__(self) -> None:
        super().__init__()
        self.name = "pacman"

    def get_pacman_pkgs(
        self, command: str, repo_location: str
    ) -> dict[str, dict[str, str]]:
        """
        Обработка вывода pacman и составление словаря
        Converts lines from pacman package manager to dict
        """
        pkgs = self.get_pkgs(command)
        pkgs_data = {}
        for i in range(0, len(pkgs), 3):
            pkgs_data[pkgs[i].split(":")[1][1:]] = {
                repo_location: pkgs[i + 1].split(":")[1][1:],
                "Description": pkgs[i + 2].split(":")[1][1:],
            }
        return pkgs_data

    def install_pkgs(self, pkgs: list | tuple, passwd: str) -> None:
        super().install_pkgs("pacman -S --noconfirm", pkgs, passwd)

    def update_pkgs(self, pkgs: list | tuple, passwd: str) -> None:
        super().install_pkgs("pacman -Syu --noconfirm ", pkgs, passwd)

    def remove_pkgs(self, pkgs: list | tuple, passwd: str) -> None:
        super().remove_pkgs("pacman -R --noconfirm", pkgs, passwd)

    def clean_orphans(self, passwd: str) -> None:
        super().clean_orphans("pacman -R --noconfirm $(pacman -Qqtd)", passwd)

    def get_installed_pkgs(self) -> dict[str, dict[str, str]]:
        super().get_installed_pkgs()
        return self.get_pacman_pkgs(
            "pacman -Qi | awk '/^(Name|Version|Description)/'", "Installed"
        )

    def get_repo_pkgs(self) -> dict[str, dict[str, str]]:
        super().get_repo_pkgs()
        return self.get_pacman_pkgs(
            "pacman -Si $(pacman -Slq) | awk '/^(Name|Version|Description)/'", "Repo"
        )

    def get_pkgs_info(self) -> dict[str, dict[str, str]]:
        super()
        pkgs_data: dict = {}
        installed_pkgs = self.get_installed_pkgs()
        repo_pkgs = self.get_repo_pkgs()
        for pkg in repo_pkgs.keys():
            try:
                pkgs_data[pkg] = {
                    "Installed": installed_pkgs[pkg]["Installed"],
                    "Repo": repo_pkgs[pkg]["Repo"],
                    "Description": repo_pkgs[pkg]["Description"],
                    "Action": "None",
                }
            except KeyError:
                pkgs_data[pkg] = {
                    "Installed": "-",
                    "Repo": repo_pkgs[pkg]["Repo"],
                    "Description": repo_pkgs[pkg]["Description"],
                    "Action": "None",
                }
        return pkgs_data


if __name__ == "__main__":
    pacmanpkgm = PacmanPkgm()
