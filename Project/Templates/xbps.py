from sys import path as sys_path
from sys import exit
from os import path as os_path
from os import getcwd as os_getcwd


try:
    sys_path.insert(0, os_path.join(os_getcwd(), ".."))
    sys_path.insert(0, os_getcwd())

    from Project.Templates.template import BasePkgm

except ModuleNotFoundError as error:
    exit(f"{error.name}\n{error.path}")


class XbpsPkgm(BasePkgm):
    """
    Обработчик для пакетного менеджера xbps от void linux
    Handler for void linux xbps package manager
    """

    def __init__(self) -> None:
        super().__init__()
        self.name = "xbps"

    def xbps_separate(self, pkg_line: list[str]) -> list[str]:
        """
        [*] xdg-desktop-portal-0.7.1_1 desc... -> ['xdg-desktop-portal', '0.7.1_1', 'desc']
        """
        return [
            pkg_line[1][: pkg_line[1].rfind("-")],
            pkg_line[1][pkg_line[1].rfind("-") + 1 :],
            " ".join(pkg_line[2:]),
        ]

    def get_xbps_pkgs(
        self, command: str, repo_location: str
    ) -> dict[str, dict[str, str]]:
        """
        Обработка вывода xbps и составление словаря
        Converts lines from xbps package manager to dict
        """
        pkgs = self.get_pkgs(command)
        pkgs_data = {}
        for pkgs_info in pkgs:
            line_data = self.xbps_separate(pkgs_info.split())
            pkgs_data[line_data[0]] = {
                repo_location: line_data[1],
                "Description": line_data[2],
            }
        return pkgs_data

    def install_pkgs(self, pkgs: list | tuple, passwd: str) -> None:
        super().install_pkgs("xbps-install -y", pkgs, passwd)

    def update_pkgs(self, pkgs: list | tuple, passwd: str) -> None:
        super().install_pkgs("xbps-install -Syu ", pkgs, passwd)

    def remove_pkgs(self, pkgs: list | tuple, passwd: str) -> None:
        super().remove_pkgs("xbps-remove -y", pkgs, passwd)

    def clean_orphans(self, passwd: str) -> None:
        super().clean_orphans("xbps-remove -Roy", passwd)

    def get_installed_pkgs(self) -> dict[str, dict[str, str]]:
        super().get_installed_pkgs()
        return self.get_xbps_pkgs("xbps-query -s .", "Installed")

    def get_repo_pkgs(self) -> dict[str, dict[str, str]]:
        super().get_repo_pkgs()
        return self.get_xbps_pkgs("xbps-query -Rs .", "Repo")

    def get_pkgs_info(self) -> dict[str, dict[str, str]]:
        super()
        pkgs_data: dict = {}
        installed_pkgs = self.get_installed_pkgs()
        repo_pkgs = self.get_repo_pkgs()
        for pkg in repo_pkgs.keys():
            try:
                pkgs_data[pkg] = {
                    "Installed": installed_pkgs[pkg]["Installed"],
                    "Repo": repo_pkgs[pkg]["Repo"],
                    "Description": repo_pkgs[pkg]["Description"],
                    "Action": "None",
                }
            except KeyError:
                pkgs_data[pkg] = {
                    "Installed": "-",
                    "Repo": repo_pkgs[pkg]["Repo"],
                    "Description": repo_pkgs[pkg]["Description"],
                    "Action": "None",
                }
        return pkgs_data


if __name__ == "__main__":
    xbpspkgm = XbpsPkgm()
