from sys import path as sys_path
from sys import exit
from os import path as os_path
from os import getcwd as os_getcwd
import json


try:
    sys_path.insert(0, os_path.join(os_getcwd(), ".."))
    sys_path.insert(0, os_getcwd())

    from Project.CommonTools import sh

except ModuleNotFoundError as error:
    exit(f"{error.msg}\n{error.path}")


class BasePkgm:
    """
    Шаблон для класса пакетного менеджера
    Template for the package manager class
    """

    def __init__(self) -> None:
        self.name: str = ""

    def get_pkgs(self, command: str) -> list[str]:
        """
        Получение информации о пакетах
        Get an information about packages
        """
        return sh(command).stdout.splitlines()

    def get_installed_pkgs(self) -> dict[str, dict[str, str]]:
        """
        Получить список установленных пакетов
        Get a list of installed packages
        """
        return {
            "Package Name": {"Installed": "version", "Repo": "-", "Description": "desc"}
        }

    def get_repo_pkgs(self) -> dict[str, dict[str, str]]:
        """
        Получить список пакетов из репозиориев
        Get a list of packages from repositories
        """
        return {
            "Package Name": {"Installed": "-", "Repo": "version", "Description": "desc"}
        }

    def get_pkgs_info(self) -> dict[str, dict[str, str]]:
        return {
            "Package Name": {"Installed": "-", "Repo": "version", "Description": "desc"}
        }

    def change_pkgs(self, command: str, pkgs: list | tuple, passwd: str) -> list[str]:
        """
        Выолнение команды пакетного менеджера
        Executing a package manager command
        """
        change_cmd = f"echo '{passwd}' | sudo -S {command} {' '.join(pkgs)}"
        return sh(change_cmd).stdout.splitlines()

    def install_pkgs(self, command: str, pkgs: list | tuple, passwd: str) -> None:
        """
        Установка пакетов
        Install packages
        """
        self.change_pkgs(command, pkgs, passwd)

    def update_pkgs(self, command: str, pkgs: list | tuple, passwd: str) -> None:
        """
        Обновление пакетов
        Update packages
        """
        self.change_pkgs(command, pkgs, passwd)

    def remove_pkgs(self, command: str, pkgs: list | tuple, passwd: str) -> None:
        """
        Удаление пакетов
        Remove packages
        """
        self.change_pkgs(command, pkgs, passwd)

    def clean_orphans(self, command: str, passwd: str) -> None:
        """
        Уделение пакетов-сирот
        Remove orphan packages
        """
        change_cmd = f"echo '{passwd}' | sudo -S {command}"
        sh(change_cmd)

    def dump_pkgs_data(self):
        """
        Записать информации о пакетах
        Dump packages information
        """
        with open("./Project/Data/pkgs.jsonc", "w") as pkgs:
            info = self.get_pkgs_info()
            json.dump(info, pkgs, indent=6)
