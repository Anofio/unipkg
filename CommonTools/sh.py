#!/usr/bin/env python

import subprocess


def sh(command: str) -> subprocess.CompletedProcess[str]:
    return subprocess.run(
        command, shell=True, universal_newlines=True, stdout=subprocess.PIPE
    )
