from unipkg import UniPackageManager
from fetch import SysFetch


def main() -> None:
    system_info = SysFetch()
    system_info.reconfigure()
    unipkg = UniPackageManager(system_info.pkgm)
    pkgs = unipkg.get_installed_pkgs()
    print(pkgs)


if __name__ == "__main__":
    main()
