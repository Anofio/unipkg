from Project.fetch import SysFetch
from Project.UI import MainWindow
from PyQt6.QtWidgets import QApplication
from sys import exit as sys_exit
from sys import argv as sys_argv


class UnipkgGUI:
    def __init__(self):
        self.mainwindow = MainWindow()

    def process_ui(self) -> None:
        self.mainwindow.init_ui()


def main() -> None:
    # Обновлени информации о системе
    # Update system info
    fetch = SysFetch()
    fetch.reconfigure()

    # Запуск главного окна
    # Show main main window
    app = QApplication(sys_argv)
    unipkg_gui = UnipkgGUI()
    unipkg_gui.mainwindow.show()
    unipkg_gui.process_ui()

    sys_exit(app.exec())


main()
